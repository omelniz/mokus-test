var gulp        = require("gulp");
var path        = require("./path");
var browserSync = require("browser-sync").create();

gulp.task("server", function () {
    browserSync.init({
        server: {
            baseDir: path.to.destination
        }
    });

    gulp
    .watch(path.to.destination + "/**/*.css")
    .on("change", browserSync.reload);
});