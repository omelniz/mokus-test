var gulp   = require("gulp");
var path   = require("./path");
var concat = require("gulp-concat");

gulp.task("js-libs", function() {
    gulp.src([

    ])
    .pipe(concat("libs.js"))
    .pipe(gulp.dest(path.to.scripts.dest));
});

gulp.task("js-source", function() {
    gulp.src([

    ])
    .pipe(concat("script.js"))
    .pipe(gulp.dest(path.to.scripts.dest));
});

gulp.task("scripts", ["js-libs", "js-source"]);